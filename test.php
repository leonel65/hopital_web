<?php

require('vendor/autoload.php');


use Spipu\Html2Pdf\Html2Pdf;
use Spipu\Html2Pdf\Exception\Html2PdfException;
use Spipu\Html2Pdf\Exception\ExceptionFormatter;

try {
    ob_start();
    ?>
        <div class="logo"><img src="images/hospital.png" alt="" style="height: 52px;"><span style="font-size:25px;padding-top:-60px;color:#1A4661">CHU Ouagadougou</span></div>;  
        <div style="padding-top: 150px;color:#883048;text-align:center;font-size:35px">RENDEZ-VOUS EN LIGNE</div>
        <hr>
        <div style="padding-top: 175px;font-size:24px;text-align: justify;">
            Monsieur / Madame <?php echo $_GET['nom'].' '. $_GET['prenom'];?><br>
            <p style="line-height: 35px;">Je fais suite a votre demande de rendez au cours du quel nous avons convenu d'un rendez-vous dont le motif est le suivant:"<?php echo $_GET['motif'];?>" prévu le <?php echo $_GET['date'];?>. </p>
        </div>
        <div style="padding-top: 150px;padding-left:600px; float: right;">
        <img  src="images/signature.png" alt="" style="height: 52px;">
        <p style="font-size: 15px;">Dr <?php echo $_GET['med'];?></p>
        </div>
    <?php
   $content = ob_get_clean();
    $html2pdf = new Html2Pdf('P', 'A4', 'fr');
    $html2pdf->setDefaultFont('Arial');
    $html2pdf->writeHTML($content);
    $html2pdf->output('rdv.pdf');
} catch (Html2PdfException $e) {
    $html2pdf->clean();

    $formatter = new ExceptionFormatter($e);
    echo $formatter->getHtmlMessage();
}

?>