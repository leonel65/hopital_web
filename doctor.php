<?php
include("adheader.php");
include("dbconnection.php");
$con=new mysqli("localhost","root","","hopital");
if ($con->connect_error) {
	die("connection failled :".$con->connect_error);
}
$err='';

	if ( isset($_POST['nom']) and !empty($_POST['nom']) &&
		 isset($_POST['prenom']) and !empty($_POST['prenom']) &&
		 isset($_POST['mail']) and !empty($_POST['mail']) &&
		 isset($_POST['contact']) and !empty($_POST['contact']) &&
		 isset($_POST['password1']) and !empty($_POST['password1'])  &&
		 isset($_POST['password']) and !empty($_POST['password'])  &&
		 isset($_POST['status']) and !empty($_POST['status'])  

		 )

		{
			$nom=$_POST['nom'];
			$prenom=$_POST['prenom'];
			$mail=$_POST['mail'];
			$sexe=$_POST['sexe'];
			$specialite=$_POST['specialite'];
			$contact=$_POST['contact'];
			$password1=$_POST['password1'];
			$password=$_POST['password'];
			$status=$_POST['status'];
			if ($password != $password1) {
																									
				  $err="<div class='alert alert-warning alert-dismissible fade show' role='alert'>
				  <strong>Enregistrement non éffectué!</strong>
				  <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
					<span aria-hidden='true'>&times;</span>
				  </button>
				</div>";
			}
			else {
				$password=sha1($password);
				$req = "INSERT INTO `medecin` (nom_med,prenom_med,specialite,contact,email,mdp,sexe,stat) VALUES ('$nom','$prenom','$specialite','$contact','$mail','$password','$sexe','$status')";
				if ($con->query($req)===true) {
					$err="<div class='alert alert-warning alert-dismissible fade show' role='alert'>
					<strong>Enregistrement éffectué!</strong>
					<button type='button' class='close' data-dismiss='alert' aria-label='Close'>
					  <span aria-hidden='true'>&times;</span>
					</button>
				  </div>";				}
				else{
					$err="<div class='alert alert-warning alert-dismissible fade show' role='alert'>
					<strong>Erreur, veuillez rééssayer!</strong>
					<button type='button' class='close' data-dismiss='alert' aria-label='Close'>
					  <span aria-hidden='true'>&times;</span>
					</button>
				  </div>";
				}
			}
		}
?>

<center><h2 style="font-family:Texturina;color: #123456;margin-top:15px;">Ajouter un medecin <span class="fa fa-user-plus"></span></h2></center><hr>
		<p><?php echo $err;?></p>
<div class="container">
	<div class="row">
		<p class="un"></p>
		<form action="" method="POST" class="form-group">
			<div class="col-md-6" style="padding-top: 25px;">
			<input required style="border:1px solid transparent;margin-bottom:25px;height: 35px; width: 80%; border-bottom:1px solid #78aabb;background-color: transparent;font-family:Texturina" type="text"  name="nom" id="nom" placeholder="Nom"><br>
			<select  name="sexe" id="" style="border:1px solid transparent;margin-bottom:25px;height: 35px; width: 80%; border-bottom:1px solid #78aabb;background-color: transparent;font-family:Texturina">
			<option value="" disabled selected>Sexe</option>
				<option  value="masculin">Masculin</option>
				<option value="feminin">Féminin</option>
			</select><br>
			
			<input required style="border:1px solid transparent;height: 35px;margin-bottom:25px; width: 80%; border-bottom:1px solid #78aabb;background-color: transparent;font-family:Texturina" type="email"  name="mail" id="mail" placeholder="email">
			<input required style="border:1px solid transparent;height: 35px;margin-bottom:25px; width: 80%; border-bottom:1px solid #78aabb;background-color: transparent;font-family:Texturina" type="password"  name="password1" minlength="8" id="password1" placeholder="Password">
	
		</div>
			<div class="col-md-6" style="border-left: 1px solid #123456;padding-left:100px">
			<input required style="border:1px solid transparent;margin-bottom:25px;height: 35px; width: 80%; border-bottom:1px solid #78aabb;background-color: transparent;font-family:Texturina" type="text"  name="prenom" id="prenom" placeholder="Prenom">
			<select  name="specialite" id="specialite" style="border:1px solid transparent;margin-bottom:25px;height: 35px; width: 80%; border-bottom:1px solid #78aabb;background-color: transparent;font-family:Texturina"><br>
			<option value="" disabled selected>Spécialité</option>
			<?php
						$sqldepartment= "SELECT * FROM specialite WHERE stat='1'";
						$qsqldepartment = mysqli_query($con,$sqldepartment);
						while($rsdepartment=mysqli_fetch_array($qsqldepartment))
						{
							
								echo "<option class='specialite' value='$rsdepartment[nom_specialite]'>$rsdepartment[nom_specialite]</option>";

						}
			?>
			</select><br>
			<input required style="border:1px solid transparent;height: 35px;margin-bottom:25px; width: 80%; border-bottom:1px solid #78aabb;background-color: transparent;font-family:Texturina" type="tel"  name="contact" id="contact" placeholder="Contact">
			<input required style="border:1px solid transparent;height: 35px;margin-bottom:25px; width: 80%; border-bottom:1px solid #78aabb;background-color: transparent;font-family:Texturina" type="password" minlength="8"  name="password" id="password" placeholder="Confirm password"><br>
			<select  name="status" id="" style="border:1px solid transparent;margin-bottom:25px;height: 35px; width: 80%; border-bottom:1px solid #78aabb;background-color: transparent;font-family:Texturina">
			<option value="" disabled selected>Statut</option>
			<option  value="1">Activé</option>
				<option value="0">Désactivé</option>
			</select><br>			</div>
			<div class="col-md-1 offset-8" style="padding-top: 25px;">
			<input type="submit" class="btn btn-primary" style="color: white;font-family: Texturina;" value="Enregistrer">
			</div>
		</form>
	</div>
</div>

<?php include("adfooter.php");?>
