<style>
        @font-face {
        font-family: 'Texturina';
        src:url('fonts/Texturina1.ttf') format('truetype');,
        font-weight: normal;
        font-style: normal;
        }
    </style>
  <?php include 'header.php';?>
  <!-- Bnr Header -->
  <section class="main-banner">
    <div class="tp-banner-container">
      <div class="tp-banner">
        <ul>
          
          <!-- SLIDE  -->
          <li data-transition="random" data-slotamount="7" data-masterspeed="300"  data-saveperformance="off" > 
            <!-- MAIN IMAGE --> 
            <img src="images/souriant.jpg"  alt="slider"  data-bgposition=" center center" data-bgfit="cover" data-bgrepeat="no-repeat"> 
            
            <!-- LAYER NR. 1 -->
            <div class="tp-caption sfl tp-resizeme" 
                data-x="center" data-hoffset="0" 
                data-y="center" data-voffset="-120" 
                data-speed="800" 
                data-start="800" 
                data-easing="Power3.easeInOut" 
                data-splitin="chars" 
                data-splitout="none" 
                data-elementdelay="0.03" 
                data-endelementdelay="0.4" 
                data-endspeed="300"
                style="z-index: 5; font-size:70px; font-weight:500; color:#DF6141;  max-width: auto; max-height: auto; white-space: nowrap;font-family: 'Texturina'">Bienvenue au CHU de Ouagadougou</div>
            
            <!-- LAYER NR. 2 -->
            <div class="tp-caption sfr tp-resizeme" 
                data-x="center" data-hoffset="0" 
                data-y="center" data-voffset="-60" 
                data-speed="800" 
                data-start="1000" 
                data-easing="Power3.easeInOut" 
                data-splitin="chars" 
                data-splitout="none" 
                data-elementdelay="0.03" 
                data-endelementdelay="0.1" 
                data-endspeed="300" 
                style="z-index: 6; font-size:40px; color:#0E7567; font-weight:500; white-space: nowrap;">Assurer le bien etre des patients</div>
            
            <!-- LAYER NR. 3 -->
            <div class="tp-caption sfb tp-resizeme" 
                data-x="center" data-hoffset="0" 
                data-y="center" data-voffset="0" 
                data-speed="800" 
                data-start="1200" 
                data-easing="Power3.easeInOut" 
                data-splitin="none" 
                data-splitout="none" 
                data-elementdelay="0.1" 
                data-endelementdelay="0.1" 
                data-endspeed="300" 
                style="z-index: 7;  font-size:40px; color:#0E7567; font-weight:500; max-width: auto; max-height: auto; white-space: nowrap;">Est notre priorité</div>
            
            <!-- LAYER NR. 4 -->
            <div class="tp-caption lfb tp-resizeme scroll" 
                data-x="center" data-hoffset="0" 
                data-y="center" data-voffset="120"
                data-speed="800" 
                data-start="1300"
                data-easing="Power3.easeInOut" 
                data-elementdelay="0.1" 
                data-endelementdelay="0.1" 
                data-endspeed="300" 
                data-scrolloffset="0"
                style="z-index: 8;"><a href="#." class=""></a> </div>
          </li>
          <li data-transition="random" data-slotamount="7" data-masterspeed="300"  data-saveperformance="off" > 
            <!-- MAIN IMAGE --> 
            <img src="images/t.jpg"  alt="slider"  data-bgposition=" center center" data-bgfit="cover" data-bgrepeat="no-repeat"> 
            
            <!-- LAYER NR. 1 -->
            <div class="tp-caption sfl tp-resizeme" 
                data-x="center" data-hoffset="0" 
                data-y="center" data-voffset="-120" 
                data-speed="800" 
                data-start="800" 
                data-easing="Power3.easeInOut" 
                data-splitin="chars" 
                data-splitout="none" 
                data-elementdelay="0.03" 
                data-endelementdelay="0.4" 
                data-endspeed="300"
                style="z-index: 5; font-size:70px; font-weight:500; color:#DF6141;  max-width: auto; max-height: auto; white-space: nowrap;font-family: 'Texturina'">La prise de rendez-vous en ligne</div>
            
            <!-- LAYER NR. 2 -->
            <div class="tp-caption sfr tp-resizeme" 
                data-x="center" data-hoffset="0" 
                data-y="center" data-voffset="-60" 
                data-speed="800" 
                data-start="1000" 
                data-easing="Power3.easeInOut" 
                data-splitin="chars" 
                data-splitout="none" 
                data-elementdelay="0.03" 
                data-endelementdelay="0.1" 
                data-endspeed="300" 
                style="z-index: 6; font-size:40px; color:#0E7567; font-weight:500; white-space: nowrap;">Rapide et efficace</div>
            
            <!-- LAYER NR. 3 -->
            <div class="tp-caption sfb tp-resizeme" 
                data-x="center" data-hoffset="0" 
                data-y="center" data-voffset="0" 
                data-speed="800" 
                data-start="1200" 
                data-easing="Power3.easeInOut" 
                data-splitin="none" 
                data-splitout="none" 
                data-elementdelay="0.1" 
                data-endelementdelay="0.1" 
                data-endspeed="300" 
                style="z-index: 7;  font-size:40px; color:#0E7567; font-weight:500; max-width: auto; max-height: auto; white-space: nowrap;">Pour mieux vous satisfaire</div>
            
            <!-- LAYER NR. 4 -->
            <div class="tp-caption lfb tp-resizeme scroll" 
                data-x="center" data-hoffset="0" 
                data-y="center" data-voffset="120"
                data-speed="800" 
                data-start="1300"
                data-easing="Power3.easeInOut" 
                data-elementdelay="0.1" 
                data-endelementdelay="0.1" 
                data-endspeed="300" 
                data-scrolloffset="0"
                style="z-index: 8;"><a href="#." class=""></a> </div>
          </li>
          
          <!-- SLIDE  -->
          <li data-transition="random" data-slotamount="7" data-masterspeed="300"  data-saveperformance="off" > 
            <!-- MAIN IMAGE --> 
            <img src="images/slide5.jpg"  alt="slider"  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat"> 
            
            <!-- LAYER NR. 1 -->
            <div class="tp-caption sfl tp-resizeme" 
                data-x="center" data-hoffset="0" 
                data-y="center" data-voffset="-120" 
                data-speed="800" 
                data-start="800" 
                data-easing="Power3.easeInOut" 
                data-splitin="chars" 
                data-splitout="none" 
                data-elementdelay="0.03" 
                data-endelementdelay="0.4" 
                data-endspeed="300"
                style="z-index: 5; font-size:70px; font-weight:500; color:#DF6141;  max-width: auto; max-height: auto; white-space: nowrap;font-family: 'Texturina'">Des medecins qualifiés</div>
            
            <!-- LAYER NR. 2 -->
            <div class="tp-caption sfr tp-resizeme" 
                data-x="center" data-hoffset="0" 
                data-y="center" data-voffset="-60" 
                data-speed="800" 
                data-start="1000" 
                data-easing="Power3.easeInOut" 
                data-splitin="chars" 
                data-splitout="none" 
                data-elementdelay="0.03" 
                data-endelementdelay="0.1" 
                data-endspeed="300" 
                style="z-index: 6; font-size:50px; color:#0E7567; font-weight:500; white-space: nowrap;">a votre service</div>
            
            <!-- LAYER NR. 3 -->
            <div class="tp-caption sfb tp-resizeme" 
                data-x="left" data-hoffset="400" 
                data-y="center" data-voffset="30" 
                data-speed="800" 
                data-start="1400" 
                data-easing="Power3.easeInOut" 
                data-splitin="none" 
                data-splitout="none" 
                data-elementdelay="0.1" 
                data-endelementdelay="0.1" 
                data-endspeed="300" 
                style="z-index: 7; font-size:16px; color:#000; font-weight:500; line-height:26px; max-width: auto; max-height: auto; white-space: nowrap;"></div>
            
            <!-- LAYER NR. 4 -->
            <div class="tp-caption lfb tp-resizeme scroll" 
                data-x="left" data-hoffset="400" 
                data-y="center" data-voffset="140"
                data-speed="800" 
                data-start="1600"
                data-easing="Power3.easeInOut" 
                data-elementdelay="0.1" 
                data-endelementdelay="0.1" 
                data-endspeed="300" 
                data-scrolloffset="0"
          </li>
          
          <!-- SLIDE  -->
          
            
        </ul>
      </div>
    </div>
  </section>
  
  <!-- Content -->
  <div id="content"> 
    
    <!-- Intro -->
    <section class="p-t-b-150">
      <div class="container">
        <div class="intro-main">
          <div class="row"> 
            
            <!-- Intro Detail -->
            <div class="col-md-8">
              <div class="text-sec">
                <h5>
Bilan de santé au CHU</h5>
<P>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ea iure nesciunt, odio ex architecto a in assumenda eveniet sit tenetur amet minus, molestias voluptas. Asperiores totam delectus dignissimos laudantium consequuntur.</P>
                <ul class="row">
                  <li class="col-sm-6">
                    <h6> <i class="lnr  lnr-checkmark-circle"></i> pharmacie</h6>
                    <p>Excepteur sint occaecat cupidatat non roident, 
                      sunt in culpa qui officia deserunt mollit </p>
                  </li>
                  <li class="col-sm-6">
                    <h6> <i class="lnr  lnr-checkmark-circle"></i> medecins qualifiés</h6>
                    <p>Excepteur sint occaecat cupidatat non roident, 
                      sunt in culpa qui officia deserunt mollit </p>
                  </li>
                  <li class="col-sm-6">
                    <h6> <i class="lnr  lnr-checkmark-circle"></i> rendez-vous en ligne</h6>
                    <p>Excepteur sint occaecat cupidatat non roident, 
                      sunt in culpa qui officia deserunt mollit </p>
                  </li>
                  <li class="col-sm-6">
                    <h6> <i class="lnr  lnr-checkmark-circle"></i> consultation gratuite</h6>
                    <p>Excepteur sint occaecat cupidatat non roident, 
                      sunt in culpa qui officia deserunt mollit </p>
                  </li>
                </ul>
              </div>
            </div>
            
            <!-- Intro Timing -->
            <div class="col-md-4">
              <div class="timing"> <i class="lnr lnr-clock"></i>
                <ul>
                  <li> Lundi <span>8.00 - 16.00</span></li>
                  <li> Mardi <span>8.00 - 16.00</span></li>
                  <li> Mercredi <span>8.00 - 16.00</span></li>
                  <li> Jeudi <span>8.00 - 16.00</span></li>
                  <li> Vendredi <span>8.00 - 16.00</span></li>
                  <li> Samedi <span>8.00 - 16.00</span></li>
                  <li> Dimanche <span>8.00 - 16.00</span></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    
  

    
    <!-- DOCTOR LIST -->
    <section class="p-t-b-150" style="margin-top: -100px;">
      <div class="container"> 
        
        <!-- Heading -->
        <div class="heading-block">
          <h2 style="color:#C87880">Nos Services</h2>
          <hr>
<br>        
        <!-- Services -->
        <div class="services">
          <div class="row"> 
            
            <!-- Services -->
            <div class="col-md-4">
              <article>
                <div class="media-left"> <i class="flaticon-eye-2 icon"></i> </div>
                <div class="media-body">
                  <h6>SPÉCIALISTE DES YEUX</h6>
                  <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit random text.</p>
                </div>
              </article>
            </div>
            
            <!-- Services -->
            <div class="col-md-4">
              <article>
                <div class="media-left"> <i class="flaticon-operating-room icon"></i> </div>
                <div class="media-body">
                  <h6>SALLE D'OPÉRATION</h6>
                  <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit random text.</p>
                </div>
              </article>
            </div>
            
            <!-- Services -->
            <div class="col-md-4">
              <article>
                <div class="media-left"> <i class="flaticon-icu-monitor icon"></i> </div>
                <div class="media-body">
                  <h6>SOINS INTENSIFS</h6>
                  <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit random text.</p>
                </div>
              </article>
            </div>
            
            <!-- Services -->
            <div class="col-md-4">
              <article>
                <div class="media-left"> <i class="flaticon-doctor icon"></i> </div>
                <div class="media-body">
                  <h6>CONSEIL GENETIQUE</h6>
                  <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit random text.</p>
                </div>
              </article>
            </div>
            
            <!-- Services -->
            <div class="col-md-4">
              <article>
                <div class="media-left"> <i class="flaticon-heartbeat icon"></i> </div>
                <div class="media-body">
                  <h6>PROBLEMES CARDIAQUES
</h6>
                  <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit random text.</p>
                </div>
              </article>
            </div>
            
            <!-- Services -->
            <div class="col-md-4">
              <article>
                <div class="media-left"> <i class="flaticon-stomach-2 icon"></i> </div>
                <div class="media-body">
                  <h6>
PROBLÈMES D'ESTOMAC</h6>
                  <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit random text.</p>
                </div>
              </article>
            </div>
          </div>
        </div>
      </div>
    </section>
    
    
    
    <!-- Departments -->
    <section class="p-t-b-150 padding-bottom-0" style="margin-top: -200px;">
      <div class="container"> 
        
        <!-- Heading -->
        <div class="heading-block">
          <h2 style="color:#C87880">Meilleurs départements</h2>
          <hr><br>
      </div>
      
      <!-- Department -->
      <div class="department">
        <div class="container"> 
          <!-- Tab Panel -->
          <div role="tabpanel">
            <div class="dep-sec-nav"> 
              <!-- Nav tabs -->
              <ul class="nav nav-tabs" role="tablist">
                <li role="presentation"><a href="#dental" role="tab" data-toggle="tab">Dentiste</a></li>
                <li role="presentation" class="active"><a href="#cardiology" role="tab" data-toggle="tab">Cardiologie </a></li>
                <li role="presentation"><a href="#for-disabled" role="tab" data-toggle="tab">dermatologie </a></li>
                <li role="presentation"><a href="#ophthalmology" role="tab" data-toggle="tab">Ophthalmologie </a></li>
              </ul>
            </div>
            
            <!-- Tab Content -->
            <div class="tab-content"> 
              
              <!-- Dental Depatment -->
              <div role="tabpanel" class="tab-pane fade" id="dental"> 
                <!-- Depatment Text Section -->
                <div class="dep-sec-txt">
                  <div class="row">
                    <div class="col-md-7">
                      <h5>Service <span>Dentaire</span></h5>
  <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Error, debitis. Nostrum itaque blanditiis asperiores, nam deserunt nulla ad voluptatem repudiandae reprehenderit error id sit quam, placeat, alias cupiditate laudantium fuga.</p>
                      <br>
                      
                      <!-- List Style Check -->
                      <ul class="list-style-check">
                        <li> ipsum dolor sit amet  </li>
                        <li> Error, debitis. Nostrum   </li>
                        <li> 24x7 Emergency Services </li>
                        <li> Save your Money and Time with us </li>
                        <li> Medicine Research </li>
                        <li> Blood Bank </li>
                      </ul>
                    </div>
                    
                    <!-- Doctor Img -->
                    <div class="col-md-offset-1 col-md-4">
                      <div class="doctor-img"> <img class="img-responsive" src="images/dentist.jpg" alt="" >
                        <div class="name-doc">
                          <h6>Dr Stéphane <span>Dentiste </span></h6>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              
              <!-- Cardiology Depatment -->
              <div role="tabpanel" class="tab-pane fade in active" id="cardiology"> 
                <!-- Depatment Text Section -->
                <div class="dep-sec-txt">
                  <div class="row">
                    <div class="col-md-7">
                      <h5>Services <span>Cardiologie</span></h5>
<p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Modi eius illo deserunt quod sequi. Tempore excepturi praesentium similique, aliquam laborum, dolore asperiores quaerat quae numquam ullam deserunt! Excepturi, qui voluptates.</p>                        <br>
                      
                      <!-- List Style Check -->
                      <ul class="list-style-check">
                        <li> Qualified Staff of Doctors </li>
                        <li> Feel like you are at Home Services </li>
                        <li> 24x7 Emergency Services </li>
                        <li> Save your Money and Time with us </li>
                        <li> Medicine Research </li>
                        <li> Blood Bank </li>
                      </ul>
                    </div>
                    
                    <!-- Doctor Img -->
                    <div class="col-md-offset-1 col-md-4">
                      <div class="doctor-img"> <img class="img-responsive" src="images/dentiste.jpg" alt="" >
                        <div class="name-doc">
                          <h6>Dr. Ange <span>Cardiologue </span></h6>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              
              <!-- For disabled -->
              <div role="tabpanel" class="tab-pane fade" id="for-disabled"> 
                <!-- Depatment Text Section -->
                <div class="dep-sec-txt">
                  <div class="row">
                    <div class="col-md-7">
                      <h5>Service<span>dermatologie</span>  </h5>
<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Laudantium deleniti corrupti modi omnis architecto cumque sapiente nesciunt fuga minus obcaecati soluta natus exercitationem suscipit saepe quasi sit, quaerat qui similique?</p>                        <br>
                      
                      <!-- List Style Check -->
                      <ul class="list-style-check">
                        <li> Qualified Staff of Doctors </li>
                        <li> Feel like you are at Home Services </li>
                        <li> 24x7 Emergency Services </li>
                        <li> Save your Money and Time with us </li>
                        <li> Medicine Research </li>
                        <li> Blood Bank </li>
                      </ul>
                    </div>
                    
                    <!-- Doctor Img -->
                    <div class="col-md-offset-1 col-md-4">
                      <div class="doctor-img"> <img class="img-responsive" src="images/dentist.jpg" alt="" >
                        <div class="name-doc">
                          <h6>Prof. Gaby<span>dermatologue</span></h6>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              
              <!-- Ophthalmology -->
              <div role="tabpanel" class="tab-pane fade" id="ophthalmology"> 
                <!-- Depatment Text Section -->
                <div class="dep-sec-txt">
                  <div class="row">
                    <div class="col-md-7">
                      <h5>service <span>Ophthalmologie</span> </h5>

<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ea, obcaecati exercitationem quisquam magni ipsum provident nam asperiores, delectus odit corrupti eum, commodi impedit culpa consectetur consequatur ipsam numquam adipisci necessitatibus?</p>                      <br>
                      
                      <!-- List Style Check -->
                      <ul class="list-style-check">
                        <li> Qualified Staff of Doctors </li>
                        <li> Feel like you are at Home Services </li>
                        <li> 24x7 Emergency Services </li>
                        <li> Save your Money and Time with us </li>
                        <li> Medicine Research </li>
                        <li> Blood Bank </li>
                      </ul>
                    </div>
                    
                    <!-- Doctor Img -->
                    <div class="col-md-offset-1 col-md-4">
                      <div class="doctor-img"> <img class="img-responsive" src="images/dentiste.jpg" alt="" >
                        <div class="name-doc">
                          <h6>Dr. christelle<span>Ophthalmologue</span></h6>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              
            </div>
          </div>
        </div>
      </div>
    </section>
    
   
    
    
  </div>
  
  <!-- Footer -->
<?php include 'footer.php';?>