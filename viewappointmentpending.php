<link rel="stylesheet" href="css/font-awesome.min.css">

<?php

use function PHPSTORM_META\type;

include("adformheader.php");
include("dbconnection.php");
$con=new mysqli("localhost","root","","hopital");
if ($con->connect_error) {
	die("connection failled :".$con->connect_error);
}
if(isset($_GET[delid]))
{
	$sql ="DELETE FROM medecin WHERE id_med=$_GET[delid]";
	$qsql=mysqli_query($con,$sql);
	if(mysqli_affected_rows($con) == 1)
	{
		echo "<script>alert('Medecin supprimer..');</script>";
	}
}
?>
<div class="container-fluid">
	<div class="block-header">
		<center><h2 style="font-family: Texturina;font-size:25px">Prochain rendez-vous <span class="fa fa-list-alt"></span></h2></center>

	</div>
<div class="card">
<?php $doctorid= $_SESSION[doctorid];?>

	<section class="container">
		<table class="table table-bordered table-striped table-hover js-basic-example dataTable">
			<thead>
				<tr style='font-family:Texturina;font-size:16px' >
					<td>Nom</td>
					<td>Prenom </td>
					<td>date naiss</td>
					<td>Telephone</td>
					<td>Sexe</td>
					<td>Profession</td>
					<td>Motif</td>
					<td>Date rdv</td>
				</tr>
			</thead>
			<tbody>
				
				<?php
				$a=strtotime(date("Y-m-d"));
				$d=strtotime(date("H:i:s"));
				//$date_rdv='date_rdv';
				//$b =strtotime($date_rdv);
				$sql ="SELECT idp,date_rdv,heure_rdv,motif FROM rdv WHERE id_med='$doctorid'";
                $qsql = mysqli_query($con,$sql);
				foreach ($qsql as $key) 
				{
                    $patient="SELECT * FROM patient WHERE idp='$key[idp]'";
                    $sql=mysqli_query($con,$patient);
					foreach ($sql as $rs) 
					{
						$b=strtotime($key[date_rdv]);
						$c=strtotime($key[heure_rdv]);
						if (($b>$a) OR ($b==$a AND $c>=$d)) 
					   {
							echo "<tr style='font-family:Texturina;font-size:15px'>
							<td>&nbsp;$rs[nom_p]</td>
							<td>&nbsp;$rs[prenom_p]</td>
							<td>&nbsp;$rs[date_naiss]</td>
							<td>&nbsp;$rs[telephone_p]</td>
							<td>&nbsp;$rs[sexe]</td>
							<td>&nbsp;$rs[profession]</td>
							<td>&nbsp;$key[motif]</td>
							<td>&nbsp;$key[date_rdv]</td>
							</tr>";					 
						}
						
                    }
                }
				?>      
				</tbody>
			</table>
		</section>
	</div>
</div>
	<?php
	include("adformfooter.php");
	?>