<?php include 'header.php';?>


<!-- Content -->
<div id="content">

    <!-- Intro -->
    <section class="p-t-b-150">
        <div class="container">
            <div class="intro-main">
                <div class="row">

                    <!-- Intro Detail -->
                    <div class="col-md-7">
                        <div class="text-sec padding-right-0">
                            <h5>Le CHU de Ouagadougou</h5>
                            <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Ratione voluptatum enim cupiditate, vel asperiores nisi ipsa voluptas mollitia quae illum obcaecati. Architecto quisquam corrupti omnis, nam quas est aliquid esse? Lorem ipsum dolor sit amet consectetur adipisicing elit. Quaerat, nihil rerum aut nam similique iste? Excepturi a voluptates, illum ipsum, aut enim tenetur beatae laboriosam magni fugit cupiditate sit numquam.</p>
                            <ul class="row">
                                <li class="col-sm-6">
                                    <h6> <i class="lnr  lnr-checkmark-circle"></i> EMERGENCY CASE</h6>
                                    <p>Excepteur sint occaecat cupidatat non roident,
                                        sunt in culpa qui officia deserunt mollit </p>
                                </li>
                                <li class="col-sm-6">
                                    <h6> <i class="lnr  lnr-checkmark-circle"></i> QUALIFIED DOCTORS</h6>
                                    <p>Excepteur sint occaecat cupidatat non roident,
                                        sunt in culpa qui officia deserunt mollit </p>
                                </li>
                                <li class="col-sm-6">
                                    <h6> <i class="lnr  lnr-checkmark-circle"></i> ONLINE APPOINTMENT</h6>
                                    <p>Excepteur sint occaecat cupidatat non roident,
                                        sunt in culpa qui officia deserunt mollit </p>
                                </li>
                                <li class="col-sm-6">
                                    <h6> <i class="lnr  lnr-checkmark-circle"></i> FREE MEDICAL COUNSELING</h6>
                                    <p>Excepteur sint occaecat cupidatat non roident,
                                        sunt in culpa qui officia deserunt mollit </p>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <img src="images/female.jpg" style="background-size: cover;" width="210%" alt="">
                    </div>
                </div>
            </div>
        </div>
    </section>


</div>

<!-- Footer -->
<!--======= FOOTER =========-->

<?php include 'footer.php';?>