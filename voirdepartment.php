<link rel="stylesheet" href="css/font-awesome.min.css">
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<?php
include("adformheader.php");
include("dbconnection.php");
$con=new mysqli("localhost","root","","hopital");
if ($con->connect_error) {
	die("connection failled :".$con->connect_error);
}
if(isset($_GET[delid]))
{
	$sql ="DELETE FROM specialite WHERE id_spe=$_GET[delid]";
	$qsql=mysqli_query($con,$sql);
	if(mysqli_affected_rows($con) == 1)
	{
		echo "<script>alert('Specialite supprimer..');</script>";
	}
}
?>
<div class="container-fluid">
	<div class="block-header">
		<center><h2 style="font-family: Texturina;font-size:25px">Liste Specialite <span class="fa fa-list"></span></h2></center>

	</div>

<div class="card">

	<section class="container">
		<table class="table table-bordered table-striped table-hover js-basic-example dataTable">
			<thead>
				<tr style='font-family:Texturina;font-size:15px'>
					<td>Specialite</td>
					<td>Frais</td>
					<td>Status</td>
					<td>Action</td>
				</tr>
			</thead>
			<tbody style='font-family:Texturina;font-size:15px'>
				
				<?php
				$sql ="SELECT * FROM specialite";
				$qsql = mysqli_query($con,$sql);
				while($rs = mysqli_fetch_array($qsql))
				{
					if ($rs[stat]==1) {
						$stat='Activé';
					}
					echo "<tr>
					<td>&nbsp;$rs[nom_specialite]</td>
					<td>&nbsp;$rs[prix] fcfa</td>
					<td>&nbsp;$stat</td>
					
					<td>
					<a href='department.php?editid=$rs[id_spe]'><span class='zmdi zmdi-edit'></span></a> &ensp;&ensp;
					<a href='voirdepartment.php?delid=$rs[id_spe]'><span class='zmdi zmdi-delete' style='color:red'></span></a> </td>
					</tr>";
				}
				?>      </tbody>
			</table>
		</section>
	</div>
</div>
	<?php
	include("adformfooter.php");
	?>