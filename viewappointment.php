<link rel="stylesheet" href="css/font-awesome.min.css">

<?php
include("adformheader.php");
include("dbconnection.php");

$con=new mysqli("localhost","root","","hopital");
if ($con->connect_error) {
	die("connection failled :".$con->connect_error);
}
if(isset($_GET[delid]))
{
	$sql ="DELETE FROM medecin WHERE id_med=$_GET[delid]";
	$qsql=mysqli_query($con,$sql);
	if(mysqli_affected_rows($con) == 1)
	{
		echo "<script>alert('Medecin supprimer..');</script>";
	}
}
$idp=$_SESSION['patientid'];
?>
		
<div class="container-fluid">
	<div class="block-header">
		<center><h2 style="font-family: Texturina;font-size:25px">Mes rendez-vous approuvés <span class="fa fa-list-alt"></span></h2></center>

	</div>

<div class="card">

	<section class="container">
		<table class="table table-bordered table-striped table-hover js-basic-example dataTable">
			<thead>
				<tr style='font-family:Texturina;font-size:15px'>
					<td>Nom</td>
					<td>age(ans)</td>
					<td>date rdv </td>
					<td>heure rdv</td>
					<td>specialité</td>
					<td>medecin</td>
					<td>Motifs</td>
					<td>Frais</td>
					<td>Action</td>
				</tr>
			</thead>
			<tbody>
				
				<?php
				$sql ="SELECT * FROM patient WHERE idp='$idp'";
				$qsql = mysqli_query($con,$sql);
				foreach ($qsql as $key) {
					$nom=$key['nom_p'];
					$prenom=$key['prenom_p'];
					$age=$key['date_naiss'];
					$a=date_parse($age);
					$j=date("Y")-$a['year'];
				}	

				$sql ="SELECT * FROM rdv WHERE stat='2' AND idp='$idp'";
				$qsql = mysqli_query($con,$sql);
				while($rs = mysqli_fetch_array($qsql))
				{
					$med=$rs[id_med];
					$sql ="SELECT * FROM medecin WHERE id_med='$med'";
					$sql = mysqli_query($con,$sql);
					$med=mysqli_fetch_row($sql);
					$spe=$med[3];

					$sql1 ="SELECT * FROM specialite WHERE nom_specialite='$spe'";
					$sql1 = mysqli_query($con,$sql1);
					$special=mysqli_fetch_row($sql1);
					$prix=$special[2];

						echo "
					<tr style='font-family:Texturina;font-size:15px'>
						<td>&nbsp;$prenom $nom </td>
						<td>&nbsp;$j</td>
						<td>&nbsp;$rs[date_rdv]</td>
						<td>&nbsp;$rs[heure_rdv]</td>
						<td>&nbsp;$spe</td>
						<td>&nbsp;$med[1]</td>
						<td>&nbsp;$rs[motif]</td>
						<td>&nbsp;$prix</td>
						<td><a href='test.php?nom=$nom&prenom=$prenom&motif=$rs[motif]&date=$rs[date_rdv]&med=$med[1]'><span class='fa fa-file-pdf-o' style='color:red'></span></a></td>
					</tr>";
				}
				?>      </tbody>
			</table>
		</section>
	</div>
</div>
</div>

	<?php
	include("adformfooter.php");
	?>