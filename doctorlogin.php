
<?php
session_start();

include("dbconnection.php");
$dt = date("Y-m-d");
$tim = date("H:i:s");
$err='';

$con=new mysqli("localhost","root","","hopital");
if ($con->connect_error) {
    die("connection failled :" . $con->connect_error);
}

if (
    isset($_POST['loginid']) and !empty($_POST['loginid']) &&
	isset($_POST['password']) and !empty($_POST['password']))
	{
		$sql= "SELECT * FROM medecin WHERE stat='1'";
		$alldoctor = mysqli_query($con,$sql);
		foreach ($alldoctor as $doctor) {
			$password= sha1($_POST['password']);
			if (($doctor['email']==$_POST['loginid']) and($doctor['mdp']==$password)) {
				$_SESSION[doctorid]= $doctor['id_med'];
				$doctirId=$patient['id_med'];
				header('location:doctoraccount.php');
            }
            else{
                $err="<span class='alert alert-danger'> Erreur identifiant</span>";
            }
		
		}
	}

?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<title>CHU Ouagadougou</title>
<link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
<link rel="shortcut icon" href="images/health-insurance.png" type="image/x-icon">
<link rel="icon" href="images/health-insurance.png" type="image/x-icon">	<!-- Custom Css -->
<link href="assets/css/main.css" rel="stylesheet">
<link href="assets/css/login.css" rel="stylesheet">

<!-- Swift Themes. You can choose a theme from css/themes instead of get all themes -->
<link href="assets/css/themes/all-themes.css" rel="stylesheet" />
</head>
<body class="theme-cyan login-page authentication">
<!-- header section -->


    <div class="card-top"></div>
    <div class="card">
    <p><?php echo $err;?></p>
	<center><h1 class="title"><span></span> Medecin </h1></center>
        <div class="col-md-12">

    <form method="post" action="" name="frmadminlogin" id="sign_in">
    			<div class="input-group"> <span class="input-group-addon"> <i class="zmdi zmdi-account"></i> </span>
                    <div class="form-line">
					<input type="text" name="loginid" id="loginid" class="form-control" placeholder="Email" /></div>
                </div>
                <div class="input-group"> <span class="input-group-addon"> <i class="zmdi zmdi-lock"></i> </span>
                    <div class="form-line">
					<input type="password" name="password" id="password" class="form-control"  placeholder="Password" /> </div>
                </div>
                <div>
                    <div class="">
                        <input type="checkbox" name="rememberme" id="rememberme" class="filled-in chk-col-pink">
                        <label for="rememberme">Remember Me</label>
                    </div>
                    <div class="text-center">
					<input type="submit" name="submit" id="submit" value="Login" class="btn btn-raised waves-effect g-bg-cyan" /></div>
					<div class="text-center"> <a href="forgot-password.html">Forgot Password?</a></div>

				</div>
            </form>
        </div>
    </div>    
</div>
 <div class="clear"></div>
 <div class="theme-bg"></div>
  </div>
</div>
