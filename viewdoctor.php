<link rel="stylesheet" href="css/font-awesome.min.css">
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">

<?php
include("adformheader.php");
include("dbconnection.php");
$con=new mysqli("localhost","root","","hopital");
if ($con->connect_error) {
	die("connection failled :".$con->connect_error);
}
if(isset($_GET[delid]))
{
	$sql ="DELETE FROM medecin WHERE id_med=$_GET[delid]";
	$qsql=mysqli_query($con,$sql);
	if(mysqli_affected_rows($con) == 1)
	{
		echo "<script>alert('Medecin supprimer..');</script>";
	}
}
?>
<div class="container-fluid">
	<div class="block-header">
		<center><h2 style="font-family: Texturina;font-size:25px">Liste Medecin <span class="fa fa-list-alt"></span></h2></center>

	</div>

<div class="card">

	<section class="container">
		<table class="table table-bordered table-striped table-hover js-basic-example dataTable">
			<thead>
				<tr style='font-family:Texturina;font-size:15px'>
					<td>Nom</td>
					<td>Prenom </td>
					<td>Specialite</td>
					<td>Contact </td>
					<td>Sexe</td>
					<td>Status</td>
					<td>Action</td>
				</tr>
			</thead>
			<tbody>
				
				<?php
				$sql ="SELECT * FROM medecin";
				$qsql = mysqli_query($con,$sql);
				while($rs = mysqli_fetch_array($qsql))
				{
					if ($rs[stat]=='1') {
						$etat="Activé";
					  }
					  else {
						$etat="Désactivé";
					  }
					echo "<tr style='font-family:Texturina;font-size:15px'>
					<td>&nbsp;$rs[nom_med]</td>
					<td>&nbsp;$rs[prenom_med]</td>
					<td>&nbsp;$rs[specialite]</td>
					<td>&nbsp;$rs[contact]</td>
					<td>&nbsp;$rs[sexe]</td>
					<td>$etat</td>
					<td>
					<a href='doctor.php?editid=$rs[id_med]'><span class='zmdi zmdi-edit'></span></a> &ensp;&ensp;
					<a href='viewdoctor.php?delid=$rs[id_med]'><span class='zmdi zmdi-delete' style='color:red'></span></a> </td>
					</tr>";
				}
				?>      </tbody>
			</table>
		</section>
	</div>
</div>
	<?php
	include("adformfooter.php");
	?>