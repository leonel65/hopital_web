<?php
include("dbconnection.php");
$dt = date("Y-m-d");
$tim = date("H:i:s");
$con=new mysqli("localhost","root","","hopital");
if ($con->connect_error) {
    die("connection failled :" . $con->connect_error);
}
$err='';

if (
    isset($_POST['nom']) and !empty($_POST['nom']) &&
    isset($_POST['tel']) and !empty($_POST['tel']) &&
    isset($_POST['date_naiss']) and !empty($_POST['date_naiss']) &&
    isset($_POST['mail']) and !empty($_POST['mail']) &&
    isset($_POST['password1']) and !empty($_POST['password1'])  &&
    isset($_POST['password']) and !empty($_POST['password'])  &&
    isset($_POST['prenom']) and !empty($_POST['prenom'])  &&
    isset($_POST['profession']) and !empty($_POST['profession'])  &&
    isset($_POST['sexe']) and !empty($_POST['sexe'])  &&
    isset($_POST['ville']) and !empty($_POST['ville'])

) {
    $nom = $_POST['nom'];
    $tel = $_POST['tel'];
    $date_naiss = $_POST['date_naiss'];
    $mail = $_POST['mail'];
    $password1 = $_POST['password1'];
    $password = $_POST['password'];
    $prenom = $_POST['prenom'];
    $profession = $_POST['profession'];
    $sexe = $_POST['sexe'];
    $ville = $_POST['ville'];
    if ($password != $password1) {

        echo "<script>alert('Error!!! mdp incorrect');</script>";
    } else
    {
        if($date_naiss>date("Y-m-d")){
            $err="
            <div  class=' alert alert-danger alert-dismissible fade show' role='alert'>
                <strong>Date de naissance invalide</strong>
                <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                <span aria-hidden='true'>&times;</span>
                </button>
            </div>"; 
        }
        else{

            $password = sha1($password);
        $now=date("Y-m-d H:i:s");
        $sql= "SELECT * FROM patient";
        $allpatient = mysqli_query($con,$sql);
        foreach ($allpatient as $all) {
            if ($all['email']==$mail) {
                $err="<span class='alert alert-danger'>Email deja Utilisé</span>";
            }
                
        }
        $req = "INSERT INTO `patient` (nom_p,prenom_p,date_naiss,email,telephone_p,ville,mdp,sexe,profession,creation) VALUES ('$nom','$prenom','$date_naiss','$mail','$tel','$ville','$password','$sexe','$profession','$now')";
        if ($con->query($req) === true) {
            echo "<script>window.location='patientlogin.php';</script>";
        } else {
            echo "Error:" . $req . "<br>" . $con->error;
        }  
        

        }
        

    }
}

?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>CHU Ouagadougou</title>
    <link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
    <link rel="shortcut icon" href="images/health-insurance.png" type="image/x-icon">
    <link rel="icon" href="images/health-insurance.png" type="image/x-icon"> <!-- Custom Css -->
    <link href="assets/css/main.css" rel="stylesheet">
    <link href="assets/css/login.css" rel="stylesheet">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <!-- Swift Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="assets/css/themes/all-themes.css" rel="stylesheet" />
</head>

<body class="theme-cyan login-page authentication">
    <!-- header section -->

    <style>
        @font-face {
            font-family: 'Texturina';
            src: url('fonts/Texturina1.ttf') format('truetype');
            font-weight: normal;
            font-style: normal;
        }
    </style>

    <div class="card-top"></div>
    <div class="card" style="width:780px;margin-left: -300px;">
        <p><?php echo $err;?></p>
        <center>
            <h1 class="title" style="font-family: Texturina;"> Nouveau Patient </h1>
        </center>
        <hr>
        <form action="" method="POST">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md 6">

                        <input required style="border:1px solid transparent;margin-bottom:25px;height: 35px; width: 80%; border-bottom:1px solid #78aabb;background-color: transparent;font-family:Texturina" type="text" name="nom" id="nom" placeholder="Nom"><br>
                        <input required style="border:1px solid transparent;margin-bottom:25px;height: 35px; width: 80%; border-bottom:1px solid #78aabb;background-color: transparent;font-family:Texturina" type="tel" name="tel" id="tel" placeholder="tel"><br>
                        <input required style="border:1px solid transparent;margin-bottom:25px;height: 35px; width: 80%; border-bottom:1px solid #78aabb;background-color: transparent;font-family:Texturina"  placeholder="Date naissance" class="textbox-n" type="text" name="date_naiss" id="date_naiss" onfocus="(this.type='date')"><br>
                        <input required style="border:1px solid transparent;margin-bottom:25px;height: 35px; width: 80%; border-bottom:1px solid #78aabb;background-color: transparent;font-family:Texturina" type="email" name="mail" id="mail" placeholder="Email"><br>
                        <input required style="border:1px solid transparent;margin-bottom:25px;height: 35px; width: 80%; border-bottom:1px solid #78aabb;background-color: transparent;font-family:Texturina" type="password" name="password" minlength="7" id="password" placeholder="Password"><br>

                    </div>
                    <div class="col-md-6">
                        <input required style="border:1px solid transparent;margin-bottom:25px;height: 35px; width: 80%; border-bottom:1px solid #78aabb;background-color: transparent;font-family:Texturina" type="text" name="prenom" id="prenom" placeholder="Prenom"><br>
                        <select name="ville" id="" style="border:1px solid transparent;margin-bottom:25px;height: 35px; width: 80%; border-bottom:1px solid #78aabb;background-color: transparent;font-family:Texturina">
                            <option value="" disabled selected>Region actuelle</option>
                            <option value="Adamaoua">Adamaoua</option>
                            <option value="Centre">Centre</option>
                            <option value="Est">Est</option>
                            <option value="Extreme nord">Extreme nord</option>
                            <option value="Littoral">Littoral</option>
                            <option value="Nord">Nord</option>
                            <option value="Nord Ouest">Nord Ouest</option>
                            <option value="Ouest">Ouest</option>
                            <option value="Sud">Sud</option>
                            <option value="Sud Ouest">Sud Ouest</option>
                            <option value="Autres">Autres</option>
                        </select><br>                        <input required style="border:1px solid transparent;margin-bottom:25px;height: 35px; width: 80%; border-bottom:1px solid #78aabb;background-color: transparent;font-family:Texturina" type="text" name="profession" id="profession" placeholder="Profession"><br>
                        <select name="sexe" id="" style="border:1px solid transparent;margin-bottom:25px;height: 35px; width: 80%; border-bottom:1px solid #78aabb;background-color: transparent;font-family:Texturina">
                            <option value="" disabled selected>Sexe</option>
                            <option value="masculin">Masculin</option>
                            <option value="feminin">Féminin</option>
                        </select><br>
                        <input required style="border:1px solid transparent;margin-bottom:25px;height: 35px; width: 80%; border-bottom:1px solid #78aabb;background-color: transparent;font-family:Texturina" type="password" name="password1" id="password1" minlength="7" placeholder="Confirm password"><br>


                    </div>
                </div>

                <div class="container" style="padding-top: 25px;padding-left:100px">
                    <div class="row">
                        <div class="col-md-2">
                            <a href="index.php" class="btn btn-danger" style="color: white;font-family: Texturina;">Annuler</a>
                        </div>&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;
                            <div class="col-md-2">
                                <input type="submit" class="btn btn-primary" style="color: white;font-family: Texturina;" value="Enregistrer">
                            </div>
                    </div>
                </div>



            </div>
        </form>
    </div>
    </div>
    <div class="clear"></div>
    <div class="theme-bg"></div>
    </div>
    </div>