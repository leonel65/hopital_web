<?php
include("adformheader.php");
include("dbconnection.php");
$con=new mysqli("localhost","root","","hopital");
$err='';

if (
    isset($_POST['specialite']) AND !empty($_POST['specialite']) AND
    isset($_POST['medecin']) AND !empty($_POST['medecin']) AND
    isset($_POST['daterdv']) AND !empty($_POST['daterdv']) AND
    isset($_POST['heurerdv']) AND !empty($_POST['heurerdv']) AND
    isset($_POST['motif']) AND !empty($_POST['motif']) 
    ) {
        $medecin=$_POST['medecin'];

        
        $idp=$_SESSION['patientid'];
        $date_rdv=$_POST['daterdv'];
        $heure_rdv=$_POST['heurerdv'];

        $sql= "SELECT id_med FROM medecin WHERE nom_med='$medecin'";
        $medecin = mysqli_query($con,$sql);
        foreach ($medecin as $key) {
            $medecin= $key['id_med'];
        }
        $stat=2;
        $motif=$_POST['motif'];
        if (strtotime( $date_rdv) < strtotime(date("Y-m-d"))) {
            $err="
            <div  class=' alert alert-danger alert-dismissible fade show' role='alert'>
                <strong>Verifiez votre date du rendez-vous</strong>
                <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                <span aria-hidden='true'>&times;</span>
                </button>
            </div>";        
         }
        elseif (strtotime($date_rdv) == strtotime(date("Y-m-d")) ) 
        {
            if (strtotime($heure_rdv)>=strtotime(date("H:i:s"))) 
            {
                $req = "INSERT INTO `rdv` (idp,date_rdv,heure_rdv,id_med,stat,motif) VALUES ('$idp','$date_rdv','$heure_rdv','$medecin','$stat','$motif')";
                if ($con->query($req)===true)
                {
                    $err="
                    <div class='alert alert-warning alert-dismissible fade show' role='alert'>
                        <strong>Votre rendez-vous a été enregistré!!!</strong>
                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                        <span aria-hidden='true'>&times;</span>
                        </button>
                    </div>";        
                }
                
            }
            else
                {
                $err="
                    <div class='alert alert-danger alert-dismissible fade show' role='alert'>
                        <strong>Verifiez l'heure de votre rendez-vous</strong>
                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                        <span aria-hidden='true'>&times;</span>
                        </button>
                    </div>";   
                }
                
           
        }
        elseif (strtotime($date_rdv) > strtotime(date("Y-m-d")) ) {
            $req = "INSERT INTO `rdv` (idp,date_rdv,heure_rdv,id_med,stat,motif) VALUES ('$idp','$date_rdv','$heure_rdv','$medecin','$stat','$motif')";
                if ($con->query($req)===true)
                {
                    $err="
                    <div class='alert alert-warning alert-dismissible fade show' role='alert'>
                        <strong>Votre rendez-vous a été enregistré!!!</strong>
                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                        <span aria-hidden='true'>&times;</span>
                        </button>
                    </div>";        
                }
        }
        else{
            $err="<div class='alert alert-warning alert-dismissible fade show' role='alert'>
            <strong>Erreur lors de l'enregistrement du rendez-vous!!!</strong>
            <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
              <span aria-hidden='true'>&times;</span>
            </button>
          </div>";      
          }
      }


?>
<center><h2 style="font-family:Texturina;color: #123456;margin-top:15px;">Prendre un rendez-vous <span class="fa fa-calendar"></span></h2></center><hr>
<p><?php echo $err;?></p>

<div class="container">
	<div class="row">
		<p class="un"></p>
		<form action="" method="POST" class="form-group">
			<div class="col-md-6">
                
                <select name="specialite" onchange="change_valeur();" id="specialite" style="border:1px solid transparent;margin-bottom:25px;height: 35px; width: 80%; border-bottom:1px solid #78aabb;background-color: transparent;font-family:Texturina"><br>
                <option value="" disabled selected>Spécialité</option>

                <?php
                            $sqldepartment= "SELECT * FROM specialite WHERE stat='1'";
                            $qsqldepartment = mysqli_query($con,$sqldepartment);
                            while($rsdepartment=mysqli_fetch_array($qsqldepartment))
                            {
                                    echo "<option class='specialite' value='$rsdepartment[nom_specialite]'>$rsdepartment[nom_specialite]</option>";

                            }
                ?>
                </select><br>
                
                <select name="medecin" class="medecin" onchange="price();" id="medecin" style="border:1px solid transparent;margin-bottom:25px;height: 35px; width: 80%; border-bottom:1px solid #78aabb;background-color: transparent;font-family:Texturina"><br>
                <option value="" disabled selected>Medecin</option>
                            
                </select><br>
                <input disabled required style="border:1px solid transparent;height: 35px;margin-bottom:25px; width: 80%; border-bottom:1px solid #78aabb;background-color: transparent;font-family:Texturina" type="frais" name="frais" class="frais" id="frais" placeholder="Frais">
		    </div>

            <div class="col-md-6" style="border-left: 1px solid #123456;padding-left:100px">
            <input style="border:1px solid transparent;height: 35px;margin-bottom:25px; width: 80%; border-bottom:1px solid #78aabb;background-color: transparent;font-family:Texturina" placeholder="Date" class="textbox-n" type="text" onfocus="(this.type='date')" id="daterdv" name="daterdv">
            <input style="padding-bottom:10px;border:1px solid transparent;height: 35px;margin-bottom:25px; width: 80%; border-bottom:1px solid #78aabb;background-color: transparent;font-family:Texturina" placeholder="Heure" class="textbox-n" type="text" onfocus="(this.type='time')" id="heurerdv" name="heurerdv">
                <textarea rows="3" placeholder="Motif" name="motif" style="border-radius: 15px;font-family:Texturina; width: 350px;border:1px solid transparent;border-bottom:1px solid #78aabb;border-left:1px solid #78aabb;border-right:1px solid #78aabb;background-color: transparent;"></textarea>
            </div>
            <p style="font-family:Texturina;font-style: italic;">NB: Si vous ne connaissez pas la spécialité, veuillez selectionner <span style="color:red;font-weight: bold;" >Generaliste</span></p>
            <div class="col-md-1 offset-8" style="padding-top: 25px;">
            <button type="submit" style="font-family: Texturina;color: white;" class="btn btn-primary">
               Envoyer  <i class="fa fa-send fa-lg"></i> 
            </button>			
        </div>
	</form>
	</div>
</div>
<script>

function change_valeur() {

select = document.getElementById("specialite");
choice = select.selectedIndex;
valeur = select.options[choice].value;
$.ajax({
    url : 'aa.php', // La ressource ciblée
    type : 'GET' ,
    data : 'specialite=' + valeur,
    dataType : 'json',
    success : function(data,i){
            for(i=0; i<data.length; i++){
                $("#medecin").append(
                    "<option value="+ data[i]['nom_med']+">"
                                +'Dr '+data[i]['nom_med']+
                    "</option>"
                );
            }
        },
});
$.ajax({
    url : 'price.php', // La ressource ciblée
    type : 'GET' ,
    data : 'specialite=' + valeur,
    dataType : 'json',
    success : function(data){
        $("#frais").val(data)      
    },
});

}



</script>
<?php
include("adformfooter.php");
?>
