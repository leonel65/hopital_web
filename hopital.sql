-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost
-- Généré le : Dim 07 fév. 2021 à 23:37
-- Version du serveur :  10.4.13-MariaDB
-- Version de PHP : 7.4.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `hopital`
--

-- --------------------------------------------------------

--
-- Structure de la table `medecin`
--

CREATE TABLE `medecin` (
  `id_med` int(11) NOT NULL,
  `nom_med` varchar(255) NOT NULL,
  `prenom_med` varchar(255) NOT NULL,
  `specialite` varchar(255) NOT NULL,
  `contact` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `mdp` varchar(255) NOT NULL,
  `sexe` varchar(255) NOT NULL,
  `stat` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `medecin`
--

INSERT INTO `medecin` (`id_med`, `nom_med`, `prenom_med`, `specialite`, `contact`, `email`, `mdp`, `sexe`, `stat`) VALUES
(8, 'Tchepnou', 'alex', 'Chirugie', '6980833450', 'alex@ucl.be', 'e0c9035898dd52fc65c41454cec9c4d2611bfb37', 'masculin', '1'),
(11, 'Njonou', 'Gaby', 'Dermatologue', '6980833450', 'gaby@gmail.com', 'b480c074d6b75947c02681f31c90c668c46bf6b8', 'masculin', '1'),
(12, 'Ganwa ', 'Brunelle', 'Dermatologue', '6980833450', 'brunelle@gmail.com', 'b480c074d6b75947c02681f31c90c668c46bf6b8', 'feminin', '1'),
(13, 'Kepseu', 'Josiane', 'Dentiste', '6980833450', 'josiane@gmail.com', 'b480c074d6b75947c02681f31c90c668c46bf6b8', 'feminin', '1'),
(14, 'Kuate', 'Martin', 'Dentiste', '6980833450', 'martin@gmail.com', 'b480c074d6b75947c02681f31c90c668c46bf6b8', 'masculin', '1'),
(16, 'Kembou', 'Vanel', 'Kinésithérapie', '6980833450', 'van@gmail.com', 'b480c074d6b75947c02681f31c90c668c46bf6b8', 'masculin', '1'),
(17, 'Tchuem', 'Nelson', 'Kinésithérapie', '6980833450', 'tchuem@gmail.com', 'b480c074d6b75947c02681f31c90c668c46bf6b8', 'masculin', '1'),
(18, 'Djoumbissie', 'Chanelle', 'Chirugie', '6980833450', 'chanelle@gmail.com', 'bd7dd0904b89c51bd026b44ad330f0869862dd2f', 'masculin', '1'),
(19, 'Mouaffo', 'leaticia', 'Dermatologue', '6980833450', 'leaticia@gmail.com', 'bd7dd0904b89c51bd026b44ad330f0869862dd2f', 'feminin', '1'),
(20, 'Mada', 'vanelle', 'Generaliste', '6980833450', 'leonel65kuaya@gmail.com', 'bd7dd0904b89c51bd026b44ad330f0869862dd2f', 'feminin', '1');

-- --------------------------------------------------------

--
-- Structure de la table `patient`
--

CREATE TABLE `patient` (
  `idp` int(11) NOT NULL,
  `nom_p` varchar(50) NOT NULL,
  `prenom_p` varchar(50) NOT NULL,
  `date_naiss` date NOT NULL,
  `email` varchar(50) NOT NULL,
  `telephone_p` varchar(13) NOT NULL,
  `ville` varchar(100) NOT NULL,
  `mdp` varchar(50) NOT NULL,
  `sexe` varchar(255) NOT NULL,
  `profession` varchar(255) NOT NULL,
  `stat` int(11) NOT NULL DEFAULT 1,
  `creation` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `patient`
--

INSERT INTO `patient` (`idp`, `nom_p`, `prenom_p`, `date_naiss`, `email`, `telephone_p`, `ville`, `mdp`, `sexe`, `profession`, `stat`, `creation`) VALUES
(22, 'admin', 'admin', '2001-01-18', 'admin@gmail.com', '69825822', 'yaounde', 'qsdfgsggfhjkhgfddffhjklmjfdssrsrryoiplh', 'masculin', 'etudiant', 1, '2021-01-25 10:49:47'),
(23, 'HENGUE', 'Leonel', '1997-01-30', 'leonel65kuaya@gmail.com', '698083345', 'Yaounde', 'bd7dd0904b89c51bd026b44ad330f0869862dd2f', 'masculin', 'etudiant', 1, '2021-01-25 13:51:56'),
(26, 'Chokomako', 'Dylane', '1997-02-11', 'choko@gmail.com', '698083345', 'Yaounde', 'bd7dd0904b89c51bd026b44ad330f0869862dd2f', 'masculin', 'etudiant', 1, '2021-01-27 08:15:54'),
(27, 'Chokomako', 'Dylane', '1997-02-11', 'choko@gmail.com', '698083345', 'Yaounde', 'bd7dd0904b89c51bd026b44ad330f0869862dd2f', 'masculin', 'etudiant', 0, '2021-01-27 08:15:54'),
(28, 'Beyala', 'Justine', '2000-07-26', 'justine@gmail.com', '698083345', 'Yaounde', 'bd7dd0904b89c51bd026b44ad330f0869862dd2f', 'feminin', 'etudiante', 0, '2021-01-27 09:33:16'),
(29, 'Beyala', 'Justine', '2000-07-26', 'justine@gmail.com', '698083345', 'Yaounde', 'bd7dd0904b89c51bd026b44ad330f0869862dd2f', 'feminin', 'etudiante', 0, '2021-01-27 09:33:16'),
(30, 'Beyala', 'Justine', '2000-07-26', 'justine@gmail.com', '698083345', 'Yaounde', 'bd7dd0904b89c51bd026b44ad330f0869862dd2f', 'feminin', 'etudiante', 0, '2021-01-27 09:33:16'),
(31, 'Beyala', 'Justine', '2000-07-26', 'justine@gmail.com', '698083345', 'Yaounde', 'bd7dd0904b89c51bd026b44ad330f0869862dd2f', 'feminin', 'etudiante', 0, '2021-01-27 09:33:16'),
(32, 'Amine', 'moustapha', '2021-02-04', 'amine@gmail.com', '698083345', 'Centre', 'bd7dd0904b89c51bd026b44ad330f0869862dd2f', 'masculin', 'etudiant', 1, '2021-02-05 10:50:18'),
(33, 'AGWA', 'alex', '2000-12-12', 'alex@gmail.com', '698083544', 'Est', 'bd7dd0904b89c51bd026b44ad330f0869862dd2f', 'masculin', 'etudiant', 1, '2021-02-05 11:42:21'),
(34, 'Nenba', 'Jonathan', '1998-11-24', 'jonathan@gmail.com', '698032585', 'Adamaoua', 'bd7dd0904b89c51bd026b44ad330f0869862dd2f', 'masculin', 'etudiant', 1, '2021-02-07 10:04:35');

-- --------------------------------------------------------

--
-- Structure de la table `rdv`
--

CREATE TABLE `rdv` (
  `id_rdv` int(11) NOT NULL,
  `idp` int(11) NOT NULL,
  `date_rdv` date NOT NULL,
  `heure_rdv` time NOT NULL,
  `id_med` int(11) NOT NULL,
  `stat` varchar(2) NOT NULL,
  `motif` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `rdv`
--

INSERT INTO `rdv` (`id_rdv`, `idp`, `date_rdv`, `heure_rdv`, `id_med`, `stat`, `motif`) VALUES
(7, 23, '2021-02-01', '12:30:00', 8, '1', 'aaaaa'),
(8, 23, '2021-01-27', '12:30:00', 12, '1', 'mal partout'),
(9, 27, '2021-01-28', '14:30:00', 8, '1', 'mal'),
(11, 31, '2021-01-28', '12:30:00', 14, '2', 'mal des dents'),
(12, 23, '2021-02-12', '12:30:00', 18, '2', 'fdfggfgfgf'),
(13, 32, '2021-02-05', '14:30:00', 18, '2', 'Abces'),
(14, 33, '2020-02-04', '15:30:00', 18, '2', 'brulure'),
(15, 33, '2021-12-07', '14:30:00', 19, '2', 'allergie de la peau'),
(16, 26, '2020-02-05', '12:30:00', 19, '3', 'probleme de peau'),
(18, 32, '2021-02-12', '14:30:00', 19, '2', 'probleme de peau'),
(20, 33, '2021-02-06', '19:42:00', 17, '2', 'douleur sur le corps'),
(22, 34, '2021-02-07', '11:22:55', 19, '3', 'probleme de peau'),
(23, 34, '2021-02-07', '23:30:00', 19, '2', 'malllllllllllllllllllllllllllllllllllll');

-- --------------------------------------------------------

--
-- Structure de la table `specialite`
--

CREATE TABLE `specialite` (
  `id_spe` int(11) NOT NULL,
  `nom_specialite` varchar(50) NOT NULL,
  `prix` varchar(255) NOT NULL,
  `stat` varchar(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `specialite`
--

INSERT INTO `specialite` (`id_spe`, `nom_specialite`, `prix`, `stat`) VALUES
(1, 'Generaliste', '1000', '1'),
(3, 'Dentiste', '1000', '1'),
(4, 'Dermatologue', '1500', '1'),
(6, 'Chirugie', '2000', '1'),
(7, 'Kinésithérapie', '2500', '1');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `medecin`
--
ALTER TABLE `medecin`
  ADD PRIMARY KEY (`id_med`);

--
-- Index pour la table `patient`
--
ALTER TABLE `patient`
  ADD PRIMARY KEY (`idp`);

--
-- Index pour la table `rdv`
--
ALTER TABLE `rdv`
  ADD PRIMARY KEY (`id_rdv`),
  ADD KEY `fk_id_med` (`id_med`),
  ADD KEY `fk_id_patient` (`idp`);

--
-- Index pour la table `specialite`
--
ALTER TABLE `specialite`
  ADD PRIMARY KEY (`id_spe`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `medecin`
--
ALTER TABLE `medecin`
  MODIFY `id_med` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT pour la table `patient`
--
ALTER TABLE `patient`
  MODIFY `idp` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT pour la table `rdv`
--
ALTER TABLE `rdv`
  MODIFY `id_rdv` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT pour la table `specialite`
--
ALTER TABLE `specialite`
  MODIFY `id_spe` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `rdv`
--
ALTER TABLE `rdv`
  ADD CONSTRAINT `fk_id_med` FOREIGN KEY (`id_med`) REFERENCES `medecin` (`id_med`),
  ADD CONSTRAINT `fk_id_patient` FOREIGN KEY (`idp`) REFERENCES `patient` (`idp`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
