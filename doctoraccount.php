
<?php

include("adheader.php");
include 'dbconnection.php';
$con=new mysqli("localhost","root","","hopital");

if(!isset($_SESSION[doctorid]))
{
	echo "<script>window.location='doctorlogin.php';</script>";
}

?>
<div class="container-fluid">
  <div class="block-header">
    <h1>Bienvenue <?php  $sql="SELECT * FROM `medecin` WHERE id_med='$_SESSION[doctorid]' ";
    $doctortable = mysqli_query($con,$sql);
    $doc = mysqli_fetch_array($doctortable);

    echo 'Dr. '. $doc[prenom_med].' '.$doc[nom_med]; ?>

  </h1>
</div>
</div>





<div class="card">
  <section class="container">
    <div class="row clearfix" style="margin-top: 10px">
      <div class="col-lg-3 col-md-3 col-sm-6">
        <div class="info-box-4 hover-zoom-effect">
          <div class="icon"> <i class="fa fa-users col-green"></i> </div>
          <div class="content">
            <div class="text" style="font-family: Texturina;">NOMBRE DE PATIENTS</div>
            <div class="number"><?php
                
            ?></div>
          </div>
        </div>
      </div>
      <div class="col-lg-3 col-md-3 col-sm-6">
        <div class="info-box-4 hover-zoom-effect">
          <div class="icon"> <i class="fa fa-book col-red"></i> </div>
          <div class="content">
            <div class="text" style="font-family: Texturina;">NOMBRE DE CONSULTATION</div>
            <div class="number"><?php
            $sql = "SELECT * FROM patient WHERE status='Active'";
            $qsql = mysqli_query($con,$sql);
            echo mysqli_num_rows($qsql);
            ?></div>
          </div>
        </div>
      </div>
      <div class="col-lg-3 col-md-3 col-sm-6">
        <div class="info-box-4 hover-zoom-effect">
          <div class="icon"> <i class="fa fa-bell-o fa-3x" style="color: blue;"></i> </div>
          <div class="content">
            <div class="text" style="font-family: Texturina;">RDV AUJOURD'HUI</div>
            <div class="number">
              <?php
              $sql = "SELECT * FROM appointment WHERE status='Active' AND `doctorid`=1 AND appointmentdate=' ".date("Y-m-d")."'" ;
            $qsql = mysqli_query($con,$sql);
            echo mysqli_num_rows($qsql);
            ?>
            </div>
          </div>
        </div>
      </div>
     
    </div>
  </section>
</div>



<?php
include("adfooter.php");
?>