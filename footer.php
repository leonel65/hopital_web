  <footer class="footer-2">
    <div class="container">
      <div class="row"> 
        
        <!-- Support -->
        <div class="col-sm-7">
          <h6>A-propos de nous</h6>
          <div class="about-foot">
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Et possimus blanditiis vero minus optio eligendi. Nemo aspernatur alias temporibus ea facere asperiores, dignissimos ratione, non tenetur reprehenderit laudantium, incidunt nihil!</p>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Tempora aut soluta minima rerum possimus debitis fuga nobis perferendis recusandae dolores quis inventore quod, vel porro officia distinctio et repudiandae blanditiis?</p>
          </div>
        </div>
        
       
        
        <!-- Conatct -->
        <div class="col-sm-5">
          <div class="con-info">
            <h6>Contactez nous</h6>
            <p>Leonel65</p>
            <ul>
              <li class="mobile">
                <p>+237698083345</p>
                <p>+237695344495</p>
              </li>
              <li class="email">
                <p>admin@chu-Ouagadougou.com</p>
                <p>docteur@chu-Ouagadougou.com</p>              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </footer>
  
  <!-- Rights -->
  <div class="rights style-2">
    <div class="container">
      <p>© 2021 CHU Ouagadougou</p>
    </div>
  </div>
  
  <!-- GO TO TOP  --> 
  <a href="#" class="cd-top"><i class="fa fa-angle-up"></i></a> 
  <!-- GO TO TOP  --> 
</div>
<!-- End Page Wrapper --> 

<!-- JavaScripts --> 
<script src="js/vendors/jquery/jquery.min.js"></script> 
<script src="js/vendors/wow.min.js"></script> 
<script src="js/vendors/bootstrap.min.js"></script> 
<script src="js/vendors/own-menu.js"></script> 
<script src="js/vendors/jquery.sticky.js"></script> 
<script src="js/vendors/owl.carousel.min.js"></script> 
<script src="js/vendors/jquery.magnific-popup.min.js"></script> 

<!-- SLIDER REVOLUTION 4.x SCRIPTS  --> 
<script type="text/javascript" src="rs-plugin/js/jquery.tp.t.min.js"></script> 
<script type="text/javascript" src="rs-plugin/js/jquery.tp.min.js"></script> 
<script src="js/main.js"></script>
</body>
</html>